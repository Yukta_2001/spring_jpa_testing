package coms.LibraryManagement.Tests;


import org.apache.tomcat.jni.Library;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import coms.LibraryManagement.entity.LibraryBooks;
import coms.LibraryManagement.repository.BookRepository;

import java.util.List;
import java.util.Optional;

@DataJpaTest
class LibrarySystemTests {

    @Autowired
    private BookRepository booksRepository;

    @Test
    public void saveBookTest(){
        LibraryBooks libraryBooks=LibraryBooks.builder()
                .bookName("The Commonwealth of Cricket")
                .author("Ramachandra Guha")
                .cost(1000)
                .build();
        booksRepository.save(libraryBooks);
        Assertions.assertThat(libraryBooks.getbId()).isGreaterThan(0);
    }

    @Test
    public void getBooksTest() {
        LibraryBooks libraryBooks = booksRepository.findById(1L).get();
        Assertions.assertThat(libraryBooks.getbId()).isEqualTo(0);

    }

    @Test
    public void getListOfBookTest(){
        List<LibraryBooks> libraryBooks=booksRepository.findAll();
        Assertions.assertThat(libraryBooks.size()).isGreaterThan(0);
    }

    @Test
    public void updateLibraryBookTest(){
        LibraryBooks libraryBooks = booksRepository.findById(1L).get();
        libraryBooks.setaName("Alber Gosh");
        LibraryBooks libraryBooksUpdated =  booksRepository.save(libraryBooks);
        Assertions.assertThat(libraryBooksUpdated.getaName()).isEqualTo("Alber Gosh");
    }

    @Test
    public void deleteEmployeeTest(){
        LibraryBooks libraryBooks = booksRepository.findById(1L).get();
        booksRepository.delete(libraryBooks);
        LibraryBooks libraryBooks1 = null;
        Optional<LibraryBooks> optionalBook = booksRepository.findById(1L);
        if(optionalBook.isPresent()){
            libraryBooks1 = optionalBook.get();
        }
        Assertions.assertThat(libraryBooks1).isNull();
    }



}


