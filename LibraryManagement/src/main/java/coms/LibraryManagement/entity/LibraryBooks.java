package coms.LibraryManagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="BookInfo")
public class LibraryBooks {
	@Id  // primary key
	@GeneratedValue // it will generate values automatically starts from 1
	@Column(name="bookid")
	private long bId;
	@Column(name="bookname")
	private String bName;
	@Column(name="authorname")
	private String aName;
	@Column(name="price")
	private float cost;
	
	
	
	public long getbId() {
		return bId;
	}
	public void setbId(long bId) {
		this.bId = bId;
	}
	public String getbName() {
		return bName;
	}
	public void setbName(String bName) {
		this.bName = bName;
	}
	public String getaName() {
		return aName;
	}
	public void setaName(String aName) {
		this.aName = aName;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		this.cost = cost;
	}
	public LibraryBooks() {
		super();
	}
	public LibraryBooks(long bId, String bName, String aName, float cost) {
		super();
		this.bId = bId;
		this.bName = bName;
		this.aName = aName;
		this.cost = cost;
	}
}

