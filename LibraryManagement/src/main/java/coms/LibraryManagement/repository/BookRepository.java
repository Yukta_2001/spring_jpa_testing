package coms.LibraryManagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import coms.LibraryManagement.entity.LibraryBooks;

@Repository
public interface BookRepository extends JpaRepository<LibraryBooks, Long>{
	
//	List<LibraryBooks> findByBookName(String bookName);
//    List<LibraryBooks> findByAuthor(String author);

}
